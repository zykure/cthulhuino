/*!
 * settings.h - settings manager module for cthulhu.ino
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <EEPROM.h>

//byte saveSlot = 0;
//byte loadSlot = 0;

#endif  // _SETTINGS_H_
