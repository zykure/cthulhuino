/*!
 * midi.h - midi input/output module for cthulhu.ino
 *
 * This needs the Arduino MIDI library.
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _MIDI_H
#define _MIDI_H

#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();
//boolean alwmidi = true; // for debugging monitor: false

#define MIDI_NOTE_ON 144
#define MIDI_NOTE_OFF 128

// pin used for sync-in
#define PIN_SYNC_IN 2

// pin used for BPM-tick LED
#define PIN_BPM_TICK 0

// pins used for MIDI-in/out LEDs
#define PIN_MIDI_IN 0
#define PIN_MIDI_OUT 0

// number of notes in a chord
#define CHORD_SIZE 4

// number of notes in the arpeggiator
#define ARP_LENGTH 16

// number of fields in the sequencer
#define SEQ_LENGTH 16

// number of stored chords
#define NUM_CHORDS 4

// number of arpeggiators
#define NUM_ARPS 2

// number of stored sequences
#define NUM_SEQS 4

const char notes[12][3] PROGMEM =
    { "c ", "c#", "d ", "d#", "e ", "f ", "f#", "g ", "g#", "a ", "a#", "b " };

// midi data for playback
struct {
    byte noteByte;
    byte velocityByte;

    byte noteON;
    byte noteOFF;
    byte velocity;
    byte gate;
    byte press;

    int chordPos;  // note in current chord
    int arpPos;  // chord in current arp
    int seqPos;  // arp in current seq

    boolean tie;
} player;

struct {
    byte note[NUM_CHORDS][CHORD_SIZE];  // note playback
} chords;

struct {
    byte chord[NUM_ARPS][ARP_LENGTH];  // chord playback
    byte octave[ARP_LENGTH];  // note octaves
    byte velocity[ARP_LENGTH];  // note velocities
    byte gate[ARP_LENGTH];  // gate lengths
    byte control[ARP_LENGTH];  // control signals
} arp;

struct {
    byte arp[NUM_SEQS][SEQ_LENGTH];  // arp playback
    byte seq[SEQ_LENGTH];  // song playback
} seq;

void setup_midi()
{
    MIDI.begin(MIDI_CHANNEL_OMNI);  // Listen to all incoming messages
    //MIDI.setHandleNoteOn(MIDI_NOTE_ON);
    //MIDI.setHandleNoteOff(MIDI_NOTE_OFF);
}

void midi_advance_step()
{
    // TODO
}

void midi_listen();
void midi_send();

void midi_playback()
{
    // TODO
}

void midi_listen()
{
    // TODO
}

void midi_send() {
    // TODO
}

#endif  // _MIDI_H
