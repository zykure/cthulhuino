/*!
 * menu.h - user interface module for cthulhu.ino
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _MENU_H_
#define _MENU_H_

#include "display.h"

/**
 * Pin setup:
 *   Rotary encoder
 *     GND      GND
 *     Vcc      5V
 *     SW       D5
 *     DT       D4
 *     CLK      D3
 *  Escape button
 *     Vcc      5V
 *     Out      D7
 *  4x button pad (4x 1K resistor chain)
 *     GND      GND
 *     Vcc      AREF
 *     Out      A5
 */

// number of menu pages
#define MENU_PAGES 4

// push buttons
#define BTN_ROTARY 0
#define BTN_ESCAPE 1

// pins used for push buttons
#define PIN_BUTTON_PAD A0
#define PIN_BUTTON_ESCAPE A1

// pins used for rotary encoder
#define PIN_ROTARY_A 3
#define PIN_ROTARY_B 4
#define PIN_BUTTON_ROTARY 5

// debounce time in milliseconds
const int debounce_millis PROGMEM = 100;

// translate pin state to rotary state
const int rotary_table[7][4] PROGMEM = {
  {0x0, 0x2, 0x4,  0x0},
  {0x3, 0x0, 0x1, 0x40},
  {0x3, 0x2, 0x0,  0x0},
  {0x3, 0x2, 0x1,  0x0},
  {0x6, 0x0, 0x4,  0x0},
  {0x6, 0x5, 0x0, 0x80},
  {0x6, 0x5, 0x4,  0x0},
};

// menu titles used for panel
const char menu_titles[MENU_PAGES][4] PROGMEM =
    { "SEQ", "ARP", "CHO", "GLB" };

// button codes (combinations are possible)
enum eButton
{
    bNone = 0,
    bRotary = 1,
    bEscape = 2,
    bPush1 = 2,
    bPush2 = 4,
    bPush3 = 8,
    bPush4 = 16,
};

// menu pages
enum eMenu
{
    mNone = -1,
    mSeq = 0,
    mArp = 1,
    mChord = 2,
    mGlobal = 3,
};

// button data for navigation
struct {
    int buttonState[2];
    int analogState;
    int rotaryState;
    unsigned long lastMillis;  // time of last action

    eButton code;  // button code of last action
} buttons;

// menu data
struct {
    boolean active;  // true if inside menu
    eMenu page;  // current page
} menu;

// initialize menu buttons
void setup_menu()
{
    // rotary encoder
    pinMode(PIN_ROTARY_A, INPUT);
    pinMode(PIN_ROTARY_B, INPUT);

    // rotary switch + escape button
    pinMode(PIN_BUTTON_ROTARY, INPUT_PULLUP);
    pinMode(PIN_BUTTON_ESCAPE, INPUT_PULLUP);

    menu.page = mSeq;
    menu.active = false;
}

// read a single button and return state
bool read_button(int btn)
{
    if (btn < 0 || btn > 1)
        return false;

    bool last = buttons.buttonState[btn];
    bool pressed = last;
    switch (btn) {
        case BTN_ROTARY:
            pressed = digitalRead(PIN_BUTTON_ROTARY);
            break;
        case BTN_ESCAPE:
            pressed = digitalRead(PIN_BUTTON_ESCAPE);
            break;
    }

    if (pressed != last) {
        unsigned long now = millis();
        if (pressed && now > (buttons.lastMillis + debounce_millis)) {
            TRACE("Button press: btn=%d time=%lu", btn, now);
            buttons.lastMillis = now;
        }

        buttons.buttonState[btn] = pressed;
    }

    return pressed;
}

// read analog button pad and return state
int read_analog_buttons()
{
    int last = buttons.analogState;
    int value = analogRead(PIN_BUTTON_PAD);

    int btn = 0;
    if (value > 800)
        btn = 1;
    else if (value > 400)
        btn = 2;
    else if (value > 200)
        btn = 3;
    else if (value > 100)
        btn = 4;

    if (value != last) {
        unsigned long now = millis();
        if (btn && now > (buttons.lastMillis + debounce_millis)) {
            TRACE("Button press: btn=%d time=%lu value=%d", btn, now, value);
            buttons.lastMillis = now;
        }

        buttons.analogState = last;
    }

    return btn;
}

// read rotary encoder and return translated state
int read_rotary()
{
    byte pins = (digitalRead(PIN_ROTARY_B) << 1) | digitalRead(PIN_ROTARY_A);
    int last = buttons.rotaryState & 0xf;
    int state = rotary_table[last][pins] & 0xc0;
    TRACE("Rotary turn: state=%d pins=%d", state, pins);

    buttons.rotaryState = state;
    return state;
}

void draw_menu(boolean reset);
void navigate_menu(int direction);

void process_controls()
{
    // process push buttons
    eButton button = bNone;

    int btn = read_analog_buttons();
    if (btn > 0)
        button = (eButton)(btn + bPush1);
    if (read_button(BTN_ROTARY))
        button = bRotary;
    if (read_button(BTN_ESCAPE))
        button = bEscape;

    // process rotary encoder
    int rotary = read_rotary();

    if (button & bEscape) {
        TRACE("Escape!");
        if (! menu.active) {
            // outside menu - restart sequencer
        }
        else {
            // inside menu - leave menu
            menu.active = false;
            draw_menu(true);  // reset
        }
    }
    // TODO: handle multi-button press
    else if ((button & bPush1) && (button & bPush2)) {}
    else if ((button & bPush2) && (button & bPush3)) {}
    else if ((button & bPush3) && (button & bPush4)) {}
    else if ((button & bPush4) && (button & bPush1)) {}
    else if ((button & bPush1) && (button & bPush3)) {}
    else if ((button & bPush2) && (button & bPush4)) {}
    else if (button & bPush1) {
        if (! menu.active) {
            // outside menu - play chord 1
        }
        else {
            // inside menu - go to SEQ
            menu.page = mSeq;
            draw_menu(true);  // reset
        }
    }
    else if (button & bPush2) {
        if (! menu.active) {
            // outside menu - play chord 2
        }
        else {
            // inside menu - go to ARP
            menu.page = mArp;
            draw_menu(true);  // reset
        }
    }
    else if (button & bPush3) {
        if (! menu.active) {
            // outside menu - play chord 1
        }
        else {
            // inside menu - go to CHORD
            menu.page = mChord;
            draw_menu(true);  // reset
        }
    }
    else if (button & bPush4) {
        if (! menu.active) {
            // outside menu - play chord 1
        }
        else {
            // inside menu - go to GLOB
            menu.page = mGlobal;
            draw_menu(true);  // reset
        }
    }
    else if (button & bRotary) {
        if (! menu.active) {
            // outside menu - enter menu
            menu.active = true;
            draw_menu(true);  // reset
        }
        else {
            // inside menu - navigate
            navigate_menu(0);
            draw_menu(false);
        }
    }
    else if (rotary) {
        // rotary turned
        navigate_menu(rotary == 0x40 ? -1 : 1);
        draw_menu(false);
    }
}

void navigate_menu(int direction)
{
    if (!menu.active)
        return;

    TRACE("Navigate menu: dir=%d page=%d", direction, menu.page);

    // TODO: handle navigation ...
}

void draw_menu_seq(boolean refresh);
void draw_menu_arp(boolean refresh);
void draw_menu_chords(boolean refresh);
void draw_menu_global(boolean refresh);

void draw_menu(boolean refresh)
{
    if (!menu.active)
        return;

    TRACE("Draw menu: refresh=%d page=%d", refresh, menu.page);

    switch (menu.page) {
        case mSeq:
            draw_menu_seq(refresh);
            break;
        case mArp:
            draw_menu_arp(refresh);
            break;
        case mChord:
            draw_menu_chords(refresh);
            break;
        case mGlobal:
            draw_menu_global(refresh);
            break;
        default:
            return;
            break;
    }

    if (refresh)
        display.setCursor(0, 2);
}

void draw_menu_title()
{
    // TOOD
}

void draw_menu_item(char* key, char* value)
{
    // TODO
}

void draw_menu_seq(boolean refresh) {
    // TODO
}

void draw_menu_arp(boolean refresh) {
    // TODO
}

void draw_menu_chords(boolean refresh) {
    // TODO
}

void draw_menu_global(boolean refresh) {
    // TODO
}


#endif  // _MENU_H_
