/*!
 * display.h - graphics display module for cthulhu.ino
 *
 * This needs the SSD1306Ascii library.
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _DISPLAY_H_
#define _DISPLAY_H_

//#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "logo.h"

/**
 * Pin setup:
 *   128x64 OLED
 *     GND      GND
 *     Vcc      5V
 *     SDA      SDA = A4
 *     SCL      SCL = A5
 */

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET -1 // LED reset pin (or -1 if sharing Arduino reset pin)

#define I2C_ADDRESS 0x3C

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup_display()
{
    display.begin(SSD1306_SWITCHCAPVCC, I2C_ADDRESS);

    // show Adadruit splash screen
    //display.display();

    display.setTextSize(1);
    display.cp437(true);

    display.clearDisplay();
}

void show_splash()
{
    display.clearDisplay();

    display.fillRect(0, 0, SCREEN_WIDTH, 1, WHITE);
    display.fillRect(0, 2, SCREEN_WIDTH, 2, WHITE);
    display.fillRect(0, 5, SCREEN_WIDTH, 11, WHITE);

    display.setTextColor(BLACK, WHITE);  // inverse
    display.setCursor(7, 7);
    display.println(F("... CTHULHU*ino ..."));

    display.setTextColor(WHITE);
    display.setCursor((SCREEN_WIDTH - 30) / 2, 16);
    display.print('v');
    display.print(VERSION);

    display.drawBitmap(
        (SCREEN_WIDTH - LOGO_WIDTH) / 2,
        (SCREEN_HEIGHT - LOGO_HEIGHT) - 2,
        logo_bmp, LOGO_WIDTH, LOGO_HEIGHT, INVERSE);

    display.display();
}

#ifdef WITH_TEST
void test_display()
{
    display.clearDisplay();

    // show character map
    display.setTextColor(WHITE);
    display.setCursor(0, 0);

    for (int16_t i = 0; i < 256; i++) {
        if (i == '\n')
            display.write(' ');
        else
            display.write(i);
    }

    display.display();
    delay(1000);

    display.clearDisplay();
}
#endif  // WITH_TEST

#endif  // _DISPLAY_H_
