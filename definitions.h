/*!
 * definitions.h - common settings for cthulhu.ino
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

#if defined(WITH_DEBUG) && defined(WITH_SERIAL)
#define TRACE(...) \
    { \
        char msg[128]; \
        snprintf(msg, 128, __VA_ARGS__); \
        Serial.println(msg); \
    }
#else
#define TRACE(...)
#endif

#endif  // _DEFINITIONS_H_
