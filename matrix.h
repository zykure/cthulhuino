/*!
 * matrix.h - led matrix module for cthulhu.ino
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "LedControl.h"
//#include "binary.h"

/**
 * Pin setup:
 *   8x8 matrix with MAX7219
 *     GND      GND
 *     Vcc      5V
 *     CS       D10
 *     CLK      D11
 *     DIN      D12
 */

#define PIN_LC_DIN 12
#define PIN_LC_CLK 11
#define PIN_LC_CS 10

LedControl matrix = LedControl(12, 11, 10, 1);

void setup_matrix()
{
    matrix.shutdown(0, false);
    matrix.setIntensity(0, 4);

    matrix.clearDisplay(0);
}

#ifdef WITH_TEST
void test_matrix()
{
    int i;
    int row, col;
    for(i=0; i<=8; i++) {
        for(row=0; row<8; row++) {
            for(col=0; col<8; col++) {
                    matrix.setLed(0, row, col,
                        (row == i || row == 7-i || col == 0 || col == 7-i));
            }
        }
        delay(100);
    }
    delay(1000);
}
#endif

#endif  // _MATRIX_H_
