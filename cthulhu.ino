/*!
 * cthulhu.ino - Arduino-based MIDI controller ^(;,;)^
 *
 * Developed at the Mountains of Madness. Ia ia Cthulhu fhtagn!
 * Note: This code may contain unspeakable (programming) horrors.
 *
 * The Cthulhuino MIDI controller is based on the code at:
 *   http://sinistersystems.com/cthulhinho/
 *
 * @author: Jan Behrens <zykure@protonmail.com>
 * @date: 2020-05- 01
 */

/* GUI

todo:
[ ] - limit pitch shift+jogger (arduino freeze)
[ ] - route to cc
[ ] - think about secondary arp track instead of secondary cc track (button combo to switch or maybe 32-sequence???)
[ ] - track step duration concept? (17th number)
[ ] - save load eeprom
[ ] - bufferize screen output and render when processor available (display eats cpu time -> off tempo...)


/--------------------\   /--------------------\
| SEQ arp chrds glob |   | seq ARP chrds glob |
|--------------------|   |--------------------| // [empty] = skip;
| pA:1112            |   |pos:12345678rR      | // r=rand(1,4); R=rand(1,8);  5-8 = 1-4 + oct;
| pB:12331234        |   |vol:0123456789FrR   | // F=100%, r=rand(0,50); R=rand(50,100);
| pC:55665644        |   |gat:123456789FrRT   | // F=100%; r=rand(0,50); R=rand(50,100); T=tie;       _
|                    |   |oct:BA012rR         | // B=-2; A=-1; r=rand(-1,1); R=rand(-2,2);
| SEQ:AAABABCA.(stop)|   |cc1:0123456789ABCDEF|
|                    |   |cc2:0123456789ABCDEF|
\--------------------/   \--------------------/
key combo: switch mode (A-select chord, B-select pattern)

/--------------------\  /--------------------\     (\(\
| seq arp CHRDS glob |  | seq arp chrds GLOB |     (-.-)
|--------------------|  |--------------------|     o_(")(")
| c1: a# A# G# D#    |  | BPM:128  PITCH:  0 |
| c2: g# G# G  D#    |  | bss:14V ch:1 dly:0 |    /\(O.o)/\
| c3: d# A  G# G#    |  | cc1:72 ch:2        |      / | \
| c4: c# E  G# C     |  | cc2:74 ch:2    S/L |
| c5: e  E# B  F     |  | pads ch:3          |
| c6: c# C  G  D#    |  | rst:1 seq-m:8      |
\--------------------/  \--------------------/
                          bass:
                            1,R,A = chpos (R-random, S-current_arp_value, 1234-chord_note[PITCH* independent]);   *:pitch in glob
                            4 = step length (steps required to change to next);
                            V (yes/no) = instead of midi notes, send: cc to transpose [C-note-pattern] volca bass [default:true, fixed until needed otherwise]
*/

#define VERSION 0.1

//#define WITH_TEST
//#define WITH_DEBUG
#define WITH_SERIAL

#include <TimerOne.h>

#include "definitions.h"
#include "settings.h"
#include "midi.h"
#include "display.h"
#include "matrix.h"
#include "menu.h"

#define PIN_LED 13

volatile unsigned long bars  = 0;
volatile unsigned long beats = 1;
volatile unsigned long ticks = 1;
unsigned beats_per_min = 120;  // beats per minute
unsigned ticks_per_beat = 2;  // division per beat
unsigned beats_per_bar = 4;  // division per bar

void blink(int n)
{
    int i;
    for (i = 0; i < n; i++) {
        if (i > 0)
            delay(100);
        digitalWrite(PIN_LED, HIGH);
        delay(500);
        digitalWrite(PIN_LED, LOW);
    }
}

unsigned long bpm_to_usec(unsigned bpm, unsigned div) {
    return 1000000 * 60 /  (bpm * div);
}

void dump_serial()
{
#ifdef WITH_SERIAL
    int i;
    Serial.print(F("-- at "));
    Serial.print(millis());
    Serial.print(F(" msec = "));
    Serial.print(ticks);
    Serial.print(F(" ticks"));
    Serial.println();
    for(i=0; i<=13; i++) {
        Serial.print(F("PIN D"));
        Serial.print(i);
        Serial.print(F(": \t"));
        Serial.print(digitalRead(i));
        Serial.println();
    }
    for(i=0; i<=5; i++) {
        Serial.print(F("PIN A"));
        Serial.print(i);
        Serial.print(F(": \t"));
        Serial.print(analogRead(i));
        Serial.println();
    }
    Serial.println();
#endif
}

void setup()
{
    pinMode(PIN_LED, OUTPUT);  // led
    blink(3);

#ifdef WITH_SERIAL
    Serial.begin(115200);
#endif

    setup_display();
    setup_matrix();
    //setup_menu();
    //setup_midi();

#ifdef WITH_TEST
    void test_display();
    void test_matrix();
#endif

    display.clearDisplay();
    matrix.clearDisplay(0);

    show_splash();

    //load_setting();

    delay(1000);

    Timer1.initialize(bpm_to_usec(beats_per_min, ticks_per_beat));
    Timer1.attachInterrupt(timerIsr);

    TRACE("Initialization completed at time=%lu", millis());
}

void loop()
{
#ifdef WITH_DEBUG
    //dump_serial();
#endif

    //midi_playback();
    //update_display();
    //update_matrix();

    process_controls();

    // show beats on matrix
    matrix.setLed(0, beats/8%8, beats%8, !(beats/64%2));

    // show ticks/beats on display
    display.setTextSize(2);
    display.setTextColor(WHITE, BLACK);
    display.setCursor(0, SCREEN_HEIGHT-32);
    display.println(bars);
    display.setCursor(0, SCREEN_HEIGHT-16);
    display.println(beats);

    display.display();
}

void timerIsr()
{
    // blink LED while running
    digitalWrite(PIN_LED, !(ticks%2));

    // update counters
    ticks++;
    if (ticks % ticks_per_beat == 0) {
        beats++;
        if (beats % beats_per_bar == 0) {
            bars++;
        }
    }
}

////////////////////////////////////////////////////
//                                                //
//     There was a young plebe Miskatonic,        //
//         Whose story was really ironic,         //
//            He read an old book,                //
//               And took a good look,            //
//   And now he's a high priest of the Cthonic.   //
//                                                //
////////////////////////////////////////////////////
