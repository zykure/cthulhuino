Cthulhu.ino
===========

Arduino-based MIDI controller ^(;,;)^

Developed at the Mountains of Madness. Ia ia Cthulhu fhtagn!\
Note: This code may contain unspeakable (programming) horrors.

The Cthulhuino MIDI controller is based on an idea at:
    http://sinistersystems.com/cthulhinho/
